Esta politica de privacidad aplica al uso de la aplicacion RadioCaput (�Aplicacion�) para dispositivos moviles que fue creada por el equipo de Radio Caput. La Aplicacion es un sencillo software de accesso a transmision de estaciones de radio basadas en internet\n\n

	Que informacion obtiene la aplicacion y como es usada?\n
Informacion provista por el usuario:\n
La Aplicacion no recolecta informacion provista por el usuario. \n
Informacion obtenida automaticamente:\n
La Aplicacion puede recolectar cierta informacion automaticamente incluyendo, el tipo de dispositivo movil usado, identificadores unicos de dispositivo, direccion IP del dispositivo movil, Sistema Operativo del dispositivo, navegador usado y datos de uso de la Aplicacion asi como el ID de publicidad.\n\n

	El software de terceros puede acceder a la informacion obtenida por la Aplicacion?\n
	Si, nosotros compartiremos la informacion mencionada con terceros solo en los casos descriptos anteriormente en esta politica de privacidad.\n\n

	Podremos divulgar informacion recolectada automaticamente:\n
	� Por requerimiento legal, en respuesta a una orden o proceso legal similar;\n
	� Cuando se considere en buena fe que la divulgacion es necesaria para proteger nuestros derechos, proteger nuestra seguridad o la de otros, durante investigaciones de fraude o en responder a pedidos gubernamentales;\n\n

	Tiene la Aplicacion acceso a mis contactos?\n
	No, actualmente la Aplicacion no accede a los contactos del dispositivo.\n\n

	Cuales son mis derechos de cancelacion?\n
	Ud puede detener instantaneamente la recoleccion de informacion my simplemente, desinstalando la Aplicacion. Uds. puede usar el proceso de desinstalacion estandar provisto por su dispositivo celular o via la tienda de software o red. Tambien puede requerirlo via email a at caputradio@gmail.com.\n\n


	Politica de retencion de datos. Administracion de informacion\n
	Retendremos la informacion provista por el usuario por el tiempo que la aplicacion este en uso y por una posterior cantidad de tiempo rasonable. Retendremos informacion recopilada por hasta 24 meses. Si Ud desea que la informacion provista sea removida puede solicitarlo a caputradio@gmail.com y responderemos a la brevedad. Notese que algunos o todos los datos provistos pueden ser necesarios para que la Aplicacion funcione correctamente.\n\n

	Seguridad\n
	Nos preocupamos por salvaguardar la confidencialidad de su informacion. Proseemos procesos de seguridad fisicos, electronicos y procedurales para proteger la informacion que poseemos.\n\n

	Cambios\n
	Esta politica de privacidad puede ser actualizada cada cierto tiempo por cualquier razon. Nosotros notificaremos cualquier cambio de nuestra Politica de Privacidad via la Aplicacion, email o mensaje de texto. Le aconsejamos que consulte la Politica de Privacidad periodicamente por cambios ya que el uso continuado implica aprovacion de todos los cambios. Puede consultar el historial de esta Politica haciendo click en la pagina de la Aplicacion en la tienda de Google Play. \n\n

	Su consentimiento\n
	Haciendo uso de esta Aplicacion Uds esta aceptando nuestro procesamiento de su informacion como se menciona anteriormente en esta Politica de Privacidad como la describimos. "Procesamiento" significa hacer uso de cookies en un dispositivo movil o accediendo a informacion de cualquier tipo incluyendo, pero no limitado a, recolectar, almacenar, borrar, usar, combinar o divulgar informacion que se de en la Republica Argentina. Si uds reside fuera de la Republica Argentina su informacion sera transmitida, procesada y almacenada segun los estandares de privacidad de la Republica Argentina.\n\n

	Contactenos\n
	Si Ud. tiene cualquier consulta con respecto a la privacidad durante el uso de la Aplicacion, o tiene preguntas sobre nuestras practicas, por favor contactese con nosotros via email a caputradio@gmail.com.\n\n
	


This privacy policy governs your use of the software application RadioCaput (�Application�) for mobile devices that was created by the RadioCaput team. The Application is a simple radio stream player that allows users to listen the RadioCaput internet based station\n\n

        What information does the Application obtain and how is it used?\n
User Provided Information:\n
The Application does not collects user provided information.\n
 Automatically Collected Information:\n
The Application may collect certain information automatically, including, the type of mobile device you use, your mobile devices unique device ID, the IP address of your mobile device, your mobile operating system, your Advertising ID, the type of mobile Internet browsers you use, and information about the way you use the Application.\n\n

Do third parties see and/or have access to information obtained by the Application?\n
Yes. We will share your information with third parties only in the ways that are described in this privacy statement.\n\n

        We may disclose Automatically Collected Information:\n
�	as required by law, such as to comply with a subpoena, or similar legal process;\n
�	when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request;\n\n

        Does the application has Access to my contacts?\n
Yes, but only for write access. This permission is required only to add the RadioCaput telephone number so we can provide a quick access to the RadioCaput WhatsApp chat.\n
As the application does not request read permissions for contacts, no information is collected regarding your contact.\n\n

What are my opt-out rights?\n
You can stop all collection of information by the Application easily by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network. You can also request to opt-out via email, at caputradio@gmail.com.\n\n

Data Retention Policy, Managing Your Information\n
We will retain User Provided data for as long as you use the Application and for a reasonable time thereafter. We will retain Automatically Collected information for up to 24 months and thereafter may store it in aggregate. If you�d like us to delete User Provided Data that you have provided via the Application, please contact us at caputradio@gmail.com and we will respond in a reasonable time. Please note that some or all of the User Provided Data may be required in order for the Application to function properly.\n\n

Security\n
We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain. For example, we limit access to this information to authorized employees and contractors who need to know that information in order to operate, develop or improve our Application. Please be aware that, although we endeavor provide reasonable security for information we process and maintain, no security system can prevent all potential security breaches.\n\n

Changes\n
This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new Privacy Policy here and informing you via email or text message. You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes. You can check the history of this policy by clicking here.\n\n

Your Consent\n
By using the Application, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us. "Processing,� means using cookies on a computer/hand held device or using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information, all of which activities will take place in the United States. If you reside outside the United States your information will be transferred, processed and stored there under United States privacy standards.\n\n

Contact us\n
If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at caputradio@gmail.com.\n\n